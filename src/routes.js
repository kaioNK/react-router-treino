import React from 'react'
import {Home} from './views/Home'
import {About} from './views/About'
import {TopicList} from './views/TopicList'
import {NoMatch} from './views/NoMatch'
import {TopicDetails} from './components/TopicDetails'
import {NavBar} from './components/Navbar'
import {Route, Switch, Redirect} from 'react-router-dom'

export const Routes = () => {
  return(
    <div>
      <NavBar/>
      <Switch>
        <Route exact path="/Home" component={Home} />
        <Route exact path="/">
          <Redirect to="/Home" />
        </Route>
        <Route exact path="/About" component={About} />
        <Route exact path="/About" component={Home} />
        <Route exact path="/Topics" component={TopicList} />
        <Route path="/Topics/:topicId" component={TopicDetails} />
        <Route component={NoMatch} />
      </Switch>
    </div>
  )
}
