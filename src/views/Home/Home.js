import React from 'react'

const Home = props => {
  
  console.log('*HOME => Props passado pelo Router: ', props)
  console.log("*HOME => props.location: ", props.location)
  console.log("*HOME => props.history: ", props.history)
  return(
    <div>
      <h3>Home View</h3>
    </div>
  )
}

export default Home