import React from 'react'
import {Link} from 'react-router-dom'

const TopicDetails = ({match}) => {
  console.log('TopicDetail match ', match)
  return(
    <div>
      <h3>{match.params.topicId}</h3>
      <ul>
        <li>
          <Link to="/Topics">Back to Topics</Link>
        </li>
      </ul>
    </div>
  )
}

export default TopicDetails